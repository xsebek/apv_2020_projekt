<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

$app->get('/persons', function (Request $request, Response $response, $args) {
    $stmt = $this->db->query('SELECT * FROM person WHERE active = true ORDER BY last_name');
    $tplVars['osoby'] = $stmt->fetchAll();
    return $this->view->render($response, 'persons.latte', $tplVars);
})->setName('persons');

$app->get('/persons/profile', function (Request $request, Response $response, $args) {
    $id_person = $request->getQueryParam('id_person');
    $stmt = $this->db->prepare("SELECT * FROM person WHERE id_person = :id_person");
    $stmt->bindValue(":id_person", $id_person);
    $stmt->execute();
    $tplVars['profile'] = $stmt->fetch();

    $meetstmt = $this->db->prepare("SELECT *
       FROM person_meeting LEFT JOIN (SELECT active, id_meeting, description, start, duration, location.id_location, city, street_name, street_number, zip, country FROM meeting LEFT JOIN location on meeting.id_location = location.id_location)
           AS m on person_meeting.id_meeting = m.id_meeting WHERE id_person = :id_person AND m.active = TRUE ORDER BY start");
    $meetstmt->bindValue(":id_person", $id_person);
    $meetstmt->execute();
    $tplVars['meetings'] = $meetstmt->fetchAll();

    $contactstmt = $this->db->prepare("SELECT person.id_person, person.active, id_contact, contact, name FROM person left join (SELECT * FROM contact left join contact_type on contact.id_contact_type = contact_type.id_contact_type WHERE contact.active = true)
AS m ON person.id_person = m.id_person WHERE person.id_person = :id_person");
    $contactstmt->bindValue(":id_person", $id_person);
    $contactstmt->execute();
    $tplVars['contacts'] = $contactstmt->fetchAll();
    if (empty($tplVars['profile'])) {
        exit('Person not found');
    } else {
        return $this->view->render($response, 'profile.latte', $tplVars);
    }
})->setName('person_profile');

$app->get('/persons/edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('Person is missing');
    } else {
        $stmt = $this->db->prepare('SELECT * FROM person LEFT JOIN location 
                ON (person.id_location = location.id_location) WHERE id_person = :id_person');
        $stmt->bindValue(':id_person', $params['id_person']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        if (empty($tplVars['formData'])) {
            exit('Person not found');
        } else {
            return $this->view->render($response, 'edit-person.latte', $tplVars);
        }
    }
}) ->setName('person_edit');

$app->post('/persons/edit', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam('id_person');
    $params = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare("UPDATE person SET first_name = :first_name,
                  last_name = :last_name,
                  nickname = :nickname,
                  height = :height,
                  gender = :gender,
                  birth_day = :birth_day where id_person = :id_person;");
        $stmt->bindValue(":last_name", $params['last_name']);
        $stmt->bindValue(":first_name", $params['first_name']);
        $stmt->bindValue(":nickname", $params['nickname']);
        $stmt->bindValue(":height", $params['height']);
        $stmt->bindValue(":gender", $params['gender']);
        $stmt->bindValue(":birth_day", $params['birth_day']);
        $stmt->bindValue(":id_person", $id);
        $stmt->execute();
    } catch (PDOException $exception) {
        $tplVars['message'] = 'Sorry, unable to edit this person';
        $this->logger->error($exception->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('persons'));
});

$app->post('/persons/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('Person is missing');
    } else {
        try {$stmt = $this->db->prepare("UPDATE person SET active = false WHERE id_person = :id_person");
            $stmt->bindValue(":id_person", $params['id_person']);
            $stmt->execute();
        } catch (PDOException $exception) {
            $this->logger->info($exception);
            exit('Unable to delete this person');
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('persons'));
})->setName('person_delete');

$app->get('/persons/new', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'first_name' => '',
        'last_name' => '',
        'nickname' => '',
        'id_location' => null,
        'gender' => '',
        'height' => '',
        'birth_day' => '',
        'city' => '',
        'street_name' => '',
        'street_number' => '',
        'zip' => '',
        'country' => ''
    ];
    return $this->view->render($response, 'new-person.latte', $tplVars);
})->setName('person_new');

function insert_location($db, $formData) {
    try {
        $stmt = $db->prepare("INSERT INTO location (city, street_name, street_number, zip, country) VALUES (:city, :street_name, :street_number, :zip, :country)");
        $stmt->bindValue(":city", empty($formData['city']) ? null : $formData['city']);
        $stmt->bindValue(":street_number", empty($formData['street_number']) ? null : $formData['street_number']);
        $stmt->bindValue(":street_name", empty($formData['street_name']) ? null : $formData['street_name']);
        $stmt->bindValue(":zip", empty($formData['zip']) ? null : $formData['zip']);
        $stmt->bindValue(":country", empty($formData['country']) ? null : $formData['country']);
        $stmt->execute();
        return $db->lastInsertId('location_id_location_seq');
    } catch (PDOException $exception) {
        echo $exception->getMessage();
    }
}

$app->post('/persons/new', function (Request $request, Response $response, $args){
    $formData = $request->getParsedBody();
    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname'])) {
        $tplVars['message'] = 'Fill required fields';
    } else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) || !empty($formData['zip'])) {
            $id_location = insert_location($this->db, $formData);
        }
        try {
            $stmt = $this->db->prepare("INSERT INTO person (nickname, first_name, last_name, id_location, birth_day, height, gender) VALUES 
                    (:nickname, :first_name, :last_name, :id_location, :birth_day, :height, :gender)");
            $stmt->bindValue(":nickname", $formData['nickname']);
            $stmt->bindValue(":first_name", $formData['first_name']);
            $stmt->bindValue(":last_name", $formData['last_name']);
            $stmt->bindValue(":id_location", $id_location);
            $stmt->bindValue(":gender", $formData['gender'] ? $formData['gender'] : null);
            $stmt->bindValue(":height", $formData['height'] ? $formData['height'] : null);
            $stmt->bindValue(":birth_day", $formData['birth_day'] ? $formData['birth_day'] : null);
            $stmt->execute();
            $this->logger->info('Person successfully inserted');
        } catch (PDOException $exception) {
            $tplVars['message'] = 'Sorry, that person already exists!';
            $this->logger->error($exception->getMessage());
        }
    }
    $tplVars['formData'] = $formData;
    return $response->withHeader('Location', $this->router->pathFor('persons'));
});

$app->post('/persons/search', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['search'])) {
        $tplVars['message'] = 'Fill required data for search -> Loaded full list of persons.';
        $tplVars['osoby'] = $this->db->query("SELECT * FROM person WHERE active = true ORDER BY last_name");
    } else {
        try {
            $stmt = $this->db->prepare("SELECT * FROM person WHERE (:search = lower(first_name) OR :search = lower(last_name) OR :search = lower(nickname)) ORDER BY last_name");
            $stmt->bindValue(":search", strtolower($formData['search']));
            $stmt->execute();
        } catch (PDOException $exception) {
            $tplVars['message'] = "Person can not be found";
            $this->logger->error($exception->getMessage());

        }
        $tplVars['osoby'] = $stmt->fetchAll();
    }
    return $this->view->render($response, 'persons.latte', $tplVars);
})->setName('search_person');

$app->get('/meetings', function (Request $request, Response $response, $args) {
    $stmt = $this->db->prepare("SELECT * FROM meeting WHERE active = TRUE ORDER BY id_meeting");
    $stmt->execute();
    $tplVars['meetings'] = $stmt->fetchAll();
    return $this->view->render($response, 'meetings.latte', $tplVars);
})->setName('meetings');

$app->get('/meetings/detail', function (Request $request, Response $response, $args){
    $id_meeting = $request->getQueryParam('id_meeting');
    $stmt = $this->db->prepare("SELECT * FROM meeting WHERE id_meeting = :id_meeting");
    $stmt->bindValue(":id_meeting", $id_meeting);
    $stmt->execute();
    $tplVars['meeting'] = $stmt->fetch();
    $people = $this->db->prepare("SELECT p.first_name, p.last_name FROM person_meeting left join person p on person_meeting.id_person = p.id_person WHERE id_meeting = :id_meeting AND p.active = TRUE");
    $people->bindValue(":id_meeting", $id_meeting);
    $people->execute();
    $tplVars['people'] = $people->fetchAll();

    return $this->view->render($response, 'meeting-detail.latte', $tplVars);
})->setName('meeting_detail');

$app->get('/meetings/add', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'new-meeting.latte', $args);
}) -> setName('meeting_new');

$app->post('/meetings/add', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['start'])) {
        $tplVars['message'] = 'Please fill required values';
    } else {
        $id_location = null;
        if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['street_number']) || !empty($formData['zip'])) {
            $id_location = insert_location($this->db, $formData);
        }
        try {
            $stmt = $this->db->prepare("INSERT INTO meeting (start, description, duration, id_location) VALUES 
                                        (:start, :description, :duration, :id_location)");
            $stmt->bindValue(":start", $formData['start']);
            $stmt->bindValue(":description",  $formData['description']);
            $stmt->bindValue(":duration", $formData['duration']);
            $stmt->bindValue(":id_location", $id_location);
            $stmt->execute();
            $this->logger->info("Meeting successfully inserted");
        } catch (PDOException $exception) {
            $tplVars['message'] = 'Sorry, can\'t insert that meeting';
            $this->logger->error($exception->getMessage());
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('meetings'));
});

$app->post('/meetings/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_meeting'])) {
        exit('Meeting is missing');
    } else {
        try {$stmt = $this->db->prepare("UPDATE meeting SET active = FALSE WHERE id_meeting = :id_meeting");
            $stmt->bindValue(":id_meeting", $params['id_meeting']);
            $stmt->execute();
        } catch (PDOException $exception) {
            $this->logger->info($exception);
            exit('Unable to delete this meeting');
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('meetings'));
})->setName('meeting_delete');

$app->get('/locations', function (Request $request, Response $response, $args){
    $stmt = $this->db->prepare("SELECT * FROM location ORDER BY country, city, street_name");
    $stmt->execute();
    $tplVars['locations'] = $stmt->fetchAll();

    return $this->view->render($response, 'locations.latte', $tplVars);

})->setName('locations');

$app->get('/locations/edit', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_location'])) {
        exit('Location is missing');
    } else {
        $stmt = $this->db->prepare('SELECT * FROM location WHERE id_location = :id_location');
        $stmt->bindValue(':id_location', $params['id_location']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        if (empty($tplVars['formData'])) {
            exit('Location not found');
        } else {
            return $this->view->render($response, 'edit-location.latte', $tplVars);
        }
    }
}) ->setName('location_edit');

$app->post('/locations/edit', function (Request $request, Response $response, $args){
    $id = $request->getQueryParam('id_location');
    $params = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare("UPDATE location SET city = :city,
                    street_name = :street_name,
                    street_number = :street_number,
                    zip = :zip,
                    city = :city,
                    latitude = :latitude,
                    longitude = :longitude,
                    name = :name WHERE id_location = :id_location");

        $stmt->bindValue(":city", $params['city']);
        $stmt->bindValue(":street_name", $params['street_name']);
        $stmt->bindValue(":street_number", $params['street_number']);
        $stmt->bindValue(":zip", $params['zip']);
        $stmt->bindValue(":city", $params['city']);
        $stmt->bindValue(":latitude", $params['latitude']);
        $stmt->bindValue(":longitude", $params['longitude']);
        $stmt->bindValue(":name", $params['name']);
        $stmt->bindValue(":id_location", $id);
        $stmt->execute();
    } catch (PDOException $exception) {
        $tplVars['message'] = "Unable to update this location";
        $this->logger->error($exception->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('locations'));
});

$app->get('/persons/profile/add_contact', function (Request $request, Response $response, $args) {
    $stmt = $this->db->prepare("SELECT * FROM contact_type");
    $stmt->execute();
    $tplVars['contact_type'] = $stmt->fetchAll();
    return $this->view->render($response, 'new-person-contact.latte', $tplVars);
})->setName('contact_add');

$app->post('/persons/profile/add_contact', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $formData['id_person'] = $request->getParam('id_person');
    try {
        $stmt = $this->db->prepare("INSERT INTO contact (id_person, id_contact_type, contact) VALUES (:id_person, :id_contact_type, :contact)");
        $id_stmt = $this->db->prepare("SELECT id_contact_type FROM contact_type WHERE name = :name");
        $id_stmt->bindValue(":name", $formData['contact_type']);
        $id_stmt->execute();
        $id = $id_stmt->fetch();
        $stmt->bindValue(":id_person", $formData['id_person']);
        $stmt->bindValue("id_contact_type", $id['id_contact_type']);
        $stmt->bindValue("contact", $formData['contact']);
        $stmt->execute();
        return $response->withHeader('Location', $this->router->pathFor('person_profile') . '?id_person=' . $formData['id_person']);
    } catch (PDOException $exception) {
        $this->logger->error($exception->getMessage());
    }
});

$app->post('/contact/delete', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_contact'])) {
        $this->logger->error('Missing id_contact');
        $tplVars['message'] = 'Unable to delete contact!';
    } else {
        try {
            $stmt = $this->db->prepare("UPDATE contact SET active = FALSE WHERE id_contact = :id_contact");
            $stmt->bindValue(":id_contact", $params['id_contact']);
            $stmt->execute();
        } catch (PDOException $exception) {
            $this->logger->error($exception->getMessage());
            $tplVars['message'] = 'Unable to delete contact';
        }
    }
    return $response->withHeader('Location', $this->router->pathFor('person_profile') . '?id_person=' . $params['id_person']);
}) -> setName('contact_delete');